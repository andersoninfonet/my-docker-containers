# My Docker Containers

A docker registry project 

<hr>

<h2>Oracle Database 18.4.0-xe</h2>
<p><b>give permissions to the local volume(example):</b> sudo chmod 777 /home/andysteel/Docker/oraclexe/oradata</p>
<i>docker command example to create a container:</i>
<br/><br/>
docker run --name oracle184xe \<br/>
-d \<br/>
-p 1521:1521 \<br/>
-p 5500:5500 \<br/>
-e ORACLE_SID=XE \<br/>
-e ORACLE_PDB=XEPDB1 \<br/>
-e ORACLE_PWD=oracle \<br/>
-e ORACLE_EDITION=18.4.0 \<br/>
-e ORACLE_CHARACTERSET=UTF8 \<br/>
-v /home/andysteel/Docker/oraclexe/oradata:/opt/oracle/oradata \<br/>
registry.gitlab.com/andersoninfonet/my-docker-containers/oracle-database:18.4.0-xe<br/><br/>
<i>obs: can use user sys or system for the first login</i><br/><br/>
<hr>
